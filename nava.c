#include <avr/io.h>
#include <util/delay.h>

#include "midi.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define ARRAY_END(x) (x + ARRAY_SIZE(x))

struct pin {
	volatile uint8_t *const port;
	volatile uint8_t *const ddr;
	const uint8_t mask;
} pintab[] = {
    {&PORTD, &DDRD, 1 << 5},   {&PORTD, &DDRD, 0x3 << 6},
    {&PORTA, &DDRA, 0x7 << 5}, {&PORTC, &DDRC, 0x3f << 2},
    {&PORTB, &DDRB, 1 << 4},   {&PORTB, &DDRB, 1 << 3},
    {&PORTA, &DDRA, 1 << 2},   {&PORTD, &DDRD, 1 << 4},
};

enum pin_names {
	DAC_CS,		 /* (active low) chip select of MCP4822 DAC */
	DAC_MUX_SELECT,	 /* select between 2 CD4051 analog multiplexers (IC111
			    and IC114) */
	DAC_MUX_CHANNEL, /* select one of 8 multiplexer output channels */
	LCD,		 /* 6 bits connected to LCD controller */
	LED_CS, /* (active low) enables the chain of 5 LED driver 74HC595 shift
		   registers*/
	TRIGGER_CS, /* (active low) enables the chain of 2 74HC595 drum voice
		       trigger shift registers */
	T1_OUT, /* (active low) enables the PNP transistor driver of the Trigger
		   1 output. */
	BUTTON_CS /* enables the 5 74HC165 shift registers the buttons are
		     connected to. */
};

void pin_set_multi(uint8_t p, uint8_t bits) {
	*(pintab[p].port) |= bits & pintab[p].mask;
	*(pintab[p].port) &= bits | ~pintab[p].mask;
}

void pin_set(uint8_t p, uint8_t hi) { pin_set_multi(p, hi ? 0xff : 0); }

void pin_init(void) {
	struct pin *p;
	for (p = pintab; p < ARRAY_END(pintab); p++)
		*p->ddr |= p->mask;
}

struct dac {
	uint8_t
	    multiplexer; /* 2-bit value that indicates CS lines for 2x CD4051 */
	uint8_t channel;
};

struct shift_register {
	uint8_t ic; /* Index into chain of 74HC165 ic's */
	uint8_t bit;
};

enum {
	DAC_IC111 = 1,
	DAC_IC114 = 2,
	DAC_NONE = 3,
	LED_IC116 = 0,
	LED_IC117 = 1,
	LED_IC101 = 4,
	TRIG_IC112 = 1,
	TRIG_IC113 = 0
};

struct voice {
	struct dac dac;
	struct shift_register led;
	struct shift_register trig;
	uint8_t pulse; /* Used to generate trigger pulses */
} voicetab[] = {
    {{DAC_IC114, 2}, {LED_IC117, 0}, {TRIG_IC113, 0}},
    {{DAC_IC114, 1}, {LED_IC117, 2}, {TRIG_IC113, 1}},
    {{DAC_IC114, 0}, {LED_IC117, 4}, {TRIG_IC113, 2}},
    {{DAC_IC114, 3}, {LED_IC117, 6}, {TRIG_IC113, 3}},
    {{DAC_IC114, 4}, {LED_IC116, 0}, {TRIG_IC112, 2}},
    {{DAC_IC111, 1}, {LED_IC116, 2}, {TRIG_IC112, 3}},
    {{DAC_IC111, 0}, {LED_IC116, 3}, {TRIG_IC112, 4}},
    {{DAC_IC111, 2}, {LED_IC116, 4}, {TRIG_IC112, 5}},
    {{DAC_IC111, 2}, {LED_IC116, 5}, {TRIG_IC112, 5}},
    {{DAC_IC111, 3}, {LED_IC116, 6}, {TRIG_IC112, 7}},
    {{DAC_IC111, 4}, {LED_IC116, 7}, {TRIG_IC112, 6}},
    {{DAC_NONE, 0}, {LED_IC101, 0}, {TRIG_IC113, 7}},
};

enum voicetab_entries {
	VOICE_BD,
	VOICE_SD,
	VOICE_LT,
	VOICE_MT,
	VOICE_HT,
	VOICE_RM,
	VOICE_HC,
	VOICE_CH,
	VOICE_OH,
	VOICE_CR,
	VOICE_RD,
	VOICE_T1
};

uint8_t spi_io(uint8_t data) {
	SPDR = data;
	while (!(SPSR & 1 << SPIF))
		;
	return SPDR;
}

void spi_out(uint8_t cs_pin, uint8_t *buf, int8_t n) {
	pin_set(cs_pin, 0);
	while (n--)
		spi_io(*buf++);
	pin_set(cs_pin, 1);
}

void spi_init(void) {
	enum { SDO = PORTB5, SCK = PORTB7 };

	DDRB |= 1 << SDO | 1 << SCK;
	SPCR |= 1 << SPE | 1 << MSTR;
	SPSR |= 1 << SPI2X;
}

uint8_t uint3_reverse(uint8_t x) {
	return (x & 1 ? 4 : 0) | (x & 2) | (x & 4 ? 1 : 0);
}

void dac_set_velocity(uint8_t voice, uint8_t velocity) {
	struct dac *dac = &voicetab[voice].dac;
	uint16_t data;
	uint8_t buf[2];

	/* Break before make */
	pin_set_multi(DAC_MUX_SELECT, 0x3 << 6);
	pin_set_multi(DAC_MUX_CHANNEL, uint3_reverse(dac->channel) << 5);
	pin_set_multi(DAC_MUX_SELECT, dac->multiplexer << 6);

	/* 0001 vvvvvvvvvvvv : set MCP4822 output A to value v with range
	 * 0-4.096V
	 */
	data = 0x1000 | ((32 * velocity + 31) & 0xfff);
	buf[0] = data >> 8;
	buf[1] = data & 0xff;
	spi_out(DAC_CS, buf, sizeof(buf));

	_delay_us(5); /* Allow dac to settle before voice gets triggered */
}

void dac_init(void) {
	dac_set_velocity(VOICE_BD, 127); /* for DAC trimmer adjustment */
}

void lcd_write_4(uint8_t nibble, uint8_t rs) {
	enum { LCD_PIN_E = PORTC3, LCD_PIN_RS = PORTC2 };

	uint8_t data = nibble << 4;
	if (rs)
		data |= 1 << LCD_PIN_RS;
	pin_set_multi(LCD, data);
	pin_set_multi(LCD, data | 1 << LCD_PIN_E);
	_delay_us(1);
	pin_set_multi(LCD, data);
}

enum { LCD_SLEEP_COMMAND = 40, LCD_SLEEP_COMMAND_SLOW = 1600 };

void lcd_write(uint8_t data, uint8_t rs) {
	lcd_write_4(data >> 4, rs);
	lcd_write_4(data & 0xf, rs);
	_delay_us(LCD_SLEEP_COMMAND);
}

void lcd_init() {
	_delay_us(40000);    /* LCD boot */
	lcd_write_4(0x3, 0); /* Function set */
	_delay_us(LCD_SLEEP_COMMAND);
	lcd_write(0x28, 0); /* Function set. */
	lcd_write(0x28, 0); /* Repeat function set */
	lcd_write(0x0c, 0); /* Display on, no cursor, no blink */
	lcd_write(0x01, 0); /* Display clear */
	_delay_us(LCD_SLEEP_COMMAND_SLOW);
	lcd_write(0x06, 0);		   /* Entry mode set */
	_delay_us(LCD_SLEEP_COMMAND_SLOW); /* Not sure why this is needed */
}

/* The hi-hats are a single instrument with two modes, 'open hi-hat' (0) and
 * 'closed hi-hat' (1). This variable indicates the current mode. */
static uint8_t trigger_hh_select;

void trigger_update(uint8_t delta) {
	uint8_t leds[5] = {0}, triggers[2] = {0};
	struct voice *v;

	pin_set(T1_OUT, !voicetab[VOICE_T1].pulse);

	triggers[1] = trigger_hh_select ? 1 << 1 : 0;
	for (v = voicetab; v < ARRAY_END(voicetab); v++) {
		if (v->pulse) {
			leds[v->led.ic] |= 1 << v->led.bit;
			triggers[v->trig.ic] |= 1 << v->trig.bit;
			v->pulse = v->pulse < delta ? 0 : v->pulse - delta;
		}
	}

	spi_out(TRIGGER_CS, triggers, sizeof(triggers));
	spi_out(LED_CS, leds, sizeof(leds));
}

uint8_t button_registers[5], buttons[8 * ARRAY_SIZE(button_registers)];

void button_update(uint8_t delta) {
	uint8_t i;

	/* Only update debouncing variables if non-zero time has elapsed */
	if (!delta)
		return;

	pin_set(BUTTON_CS, 1);
	for (i = 0; i < ARRAY_SIZE(button_registers); i++)
		button_registers[i] = spi_io(0);
	pin_set(BUTTON_CS, 0);

	for (i = 0; i < ARRAY_SIZE(buttons); i++)
		buttons[i] = (buttons[i] << 1) |
			     !!(button_registers[i / 8] & 1 << (i % 8));
}

/* Use the first 16 buttons to trigger MIDI notes. */
struct {
	uint8_t key, velocity;
} button_map[] = {
    {36, 100}, {36, 60},  {38, 100}, {38, 60},	{41, 100}, {41, 60},
    {45, 100}, {45, 60},  {48, 100}, {48, 60},	{37, 100}, {39, 100},
    {42, 100}, {46, 100}, {49, 100}, {51, 100},
};

/* MIDI key map from TR-909 service manual */
uint8_t midi_key_map[] = {
    VOICE_BD, /* 35 */
    VOICE_BD, /* 36 */
    VOICE_RM, /* 37 */
    VOICE_SD, /* 38 */
    VOICE_HC, /* 39 */
    VOICE_SD, /* 40 */
    VOICE_LT, /* 41 */
    VOICE_CH, /* 42 */
    VOICE_LT, /* 43 */
    VOICE_CH, /* 44 */
    VOICE_MT, /* 45 */
    VOICE_OH, /* 46 */
    VOICE_MT, /* 47 */
    VOICE_HT, /* 48 */
    VOICE_CR, /* 49 */
    VOICE_HT, /* 50 */
    VOICE_RD, /* 51 */
    VOICE_T1, /* 52 Nava "Trigger 1" output */
};

void midi_handle_note_on(uint8_t key, uint8_t velocity) {
	uint8_t voice;

	if (!velocity)
		return;

	key -= 35; /* Lowest MIDI voice number that we respond to */
	if (key >= ARRAY_SIZE(midi_key_map))
		return;

	voice = midi_key_map[key];
	voicetab[voice].pulse = 35; /* Pulse width: 2.2 ms */
	if (voice == VOICE_CH)
		trigger_hh_select = 1;
	else if (voice == VOICE_OH)
		trigger_hh_select = 0;
	dac_set_velocity(voice, velocity);
}

void midi_display_channel(uint8_t channel) {
	char *c, numbers[] = "01020304050607080910111213141516";
	lcd_write(0x80, 0); /* Cursor to upper left corner */
	for (c = "MIDI channel: "; *c; c++)
		lcd_write(*c, 1);
	lcd_write(numbers[2 * channel], 1);
	lcd_write(numbers[2 * channel + 1], 1);
}

uint8_t uart_read(uint8_t *data) {
	uint8_t status = UCSR1A;
	if (!(status & 1 << RXC1))
		return 0;

	*data = UDR1;
	return !(status & (1 << FE1 | 1 << DOR1 | 1 << UPE1));
}

int main() {
	enum { midi_channel = 12 }; /* MIDI channel 13 */
	uint8_t time;
	midi_parser parser = {0};

	pin_init();
	spi_init();
	lcd_init();
	dac_init();

	/* Run Timer0 at F_CPU/1024 Hz */
	TCCR0B = 1 << CS02 | 1 << CS00;
	time = TCNT0;

	midi_display_channel(midi_channel);

	for (;;) {
		uint8_t uart_data, i, delta;

		if (uart_read(&uart_data)) {
			midi_message msg = midi_read(&parser, uart_data);
			if (msg.status == (MIDI_NOTE_ON | midi_channel))
				midi_handle_note_on(msg.data[0], msg.data[1]);
		}

		for (i = 0; i < ARRAY_SIZE(button_map); i++)
			if (buttons[i] == 0x7f)
				midi_handle_note_on(button_map[i].key,
						    button_map[i].velocity);

		delta = TCNT0 - time;
		time += delta;
		trigger_update(delta);
		button_update(delta);
	}

	return 0;
}
